import { NavLink } from "react-router-dom";
import { FaBars, FaHome } from "react-icons/fa";
import { BiUserCircle } from "react-icons/bi";
import { AiOutlineTeam, AiOutlineUsergroupAdd } from "react-icons/ai";
import { SiProducthunt } from "react-icons/si";
import { useState } from "react";
import { AnimatePresence, motion } from "framer-motion";
import SidebarMenu from "../Sidebar/SidebarMenu";
import Footer from "../Layout/Footer";
import Header from "../Layout/Header";

const routes = [
  {
    path: "/Dashboard",
    name: "Dashboard",
    icon: <FaHome />,
  },
  {
    path: "/User",
    name: "Users",
    icon: <BiUserCircle />,
    subRoutes: [
      {
        path: "/User/Adduser",
        name: "Add User ",
        icon: <AiOutlineUsergroupAdd />,
      },
      {
        path: "/User/UserList",
        name: "List Of Users",
        icon: <AiOutlineTeam />,
      },
    ],
  },
  {
    path: "/Product",
    name: "Products",
    icon: <SiProducthunt />,
  },
];

const Sidebar = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const showAnimation = {
    hidden: {
      width: 0,
      opacity: 0,
      transition: {
        duration: 0.5,
      },
    },
    show: {
      opacity: 1,
      width: "auto",
      transition: {
        duration: 0.5,
      },
    },
  };

  return (
    <>
      <div className="main-container">
        <motion.div
          animate={{
            width: isOpen ? "200px" : "44px",

            transition: {
              duration: 0.5,
              type: "spring",
              damping: 10,
            },
          }}
          className="sidebar"
        >
          <div className="top_section">
            <AnimatePresence>
              {isOpen && (
                <motion.h1
                  variants={showAnimation}
                  initial="hidden"
                  animate="show"
                  exit="hidden"
                  className="logo"
                >
                  CRUD
                </motion.h1>
              )}
            </AnimatePresence>

            <div className="bars">
              <FaBars onClick={toggle} />
            </div>
          </div>
          <section className="routes">
            {routes.map((route, index) => {
              if (route.subRoutes) {
                return (
                  <SidebarMenu
                    setIsOpen={setIsOpen}
                    route={route}
                    showAnimation={showAnimation}
                    isOpen={isOpen}
                  />
                );
              }

              return (
                <NavLink
                  to={route.path}
                  key={index}
                  className="link"
                  activeClassName="active"
                >
                  <div className="icon">{route.icon}</div>
                  <AnimatePresence>
                    {isOpen && (
                      <motion.div
                        variants={showAnimation}
                        initial="hidden"
                        animate="show"
                        exit="hidden"
                        className="link_text"
                      >
                        {route.name}
                      </motion.div>
                    )}
                  </AnimatePresence>
                </NavLink>
              );
            })}
          </section>
        </motion.div>
        <main>
          <div>
            <div className="main">
              <Header />
              {children}
            </div>
            <Footer />
          </div>
        </main>
      </div>
    </>
  );
};

export default Sidebar;

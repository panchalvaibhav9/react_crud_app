import { Grid, InputBase, IconButton, Badge } from "@mui/material";
import NotificationsIcon from "@mui/icons-material/Notifications";
import ChatBubbleOutlineOutlinedIcon from "@mui/icons-material/ChatBubbleOutlineOutlined";
import PowerSettingsNewOutlinedIcon from "@mui/icons-material/PowerSettingsNewOutlined";
import React from "react";

const display = {
  backgroundColor: "#00073D",
};

const Header = () => {
  return (
    <div className="header">
      <Grid style={display}>
        <InputBase />
      </Grid>

      <Grid>
        <IconButton>
          <Badge badgeContent={4} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>

        <IconButton>
          <Badge badgeContent={3} color="primary">
            <ChatBubbleOutlineOutlinedIcon />
          </Badge>
        </IconButton>

        <IconButton>
          <Badge>
            <PowerSettingsNewOutlinedIcon />
          </Badge>
        </IconButton>
      </Grid>
    </div>
  );
};

export default Header;

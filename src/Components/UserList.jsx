import React, { useState, useEffect } from "react";
import {
  Grid,
  Paper,
  Typography,
  Toolbar,
  InputAdornment,
  Table,
  TableCell,
  TableBody,
  TableHead,
  TableRow,
} from "@mui/material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import SearchIcon from "@mui/icons-material/Search";
import { toast } from "react-toastify";
import CloseIcon from "@mui/icons-material/Close";
import Controls from "../Controls";
import { useNavigate } from 'react-router-dom';
import axios from "axios";

toast.configure();
const UserList = () => {
  const [apiData, setApiData] = useState([]);
  // const [confirmDialog, setConfirmDialog] = useState({ isOpen: false, title: '', subTitle: '' });
  const nav = useNavigate();

  useEffect(() => {
    axios
      .get("https://6256c4796ea70370053eff99.mockapi.io/mainapi")
      .then((res) => {
        setApiData(res.data);
        console.log(res.data);
      });
  }, [setApiData]);
  console.log(apiData.data);

  const setData = (id, name, username, email, gender, phone, city, state, password) => {
    localStorage.setItem("updateUser", JSON.stringify({ id, name, username, email, gender, phone, city, state, password }));
    nav("/User/Updateuser")
  }


  const checker = (id) => {
    if(window.confirm('Are you sure you want to delete this user?')) {
      axios.delete(`https://6256c4796ea70370053eff99.mockapi.io/mainapi/${id}`)
      .then(res => {
        console.log(res);
      })
      toast.success('User deleted successfully', {
        position: toast.POSITION.TOP_RIGHT,
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };


  return (
    <Paper>
      <Grid>
        <Typography
          variant="h3"
          component="div"
          align="center"
          style={{ margin: 25 }}
        >
          All Users List
        </Typography>
      </Grid>
      <Toolbar>
        <Controls.Input
          label="Search Users"
          style={{ width: "100%", margin: "10px 15px" }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />
      </Toolbar>
      <Table sx={{ minWidth: 650 }}>
        <TableHead>
          <TableRow>
            <TableCell>#</TableCell>
            <TableCell>Full Name</TableCell>
            <TableCell>Username</TableCell>
            <TableCell>Email ID</TableCell>
            <TableCell align="right">Gender</TableCell>
            <TableCell align="right">Phone Number</TableCell>
            <TableCell align="right">City</TableCell>
            <TableCell align="right">State</TableCell>
            <TableCell align="right">Password</TableCell>
            <TableCell align="center">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {apiData.map((item) => (
            <TableRow key={item.id}>
              <TableCell>{item.id}</TableCell>
              <TableCell>{item.name}</TableCell>
              <TableCell>{item.username}</TableCell>
              <TableCell>{item.email}</TableCell>
              <TableCell align="right">{item.gender}</TableCell>
              <TableCell align="right">{item.phone}</TableCell>
              <TableCell align="right">{item.city}</TableCell>
              <TableCell align="right">{item.state}</TableCell>
              <TableCell align="right">{item.password}</TableCell>
              <TableCell>
                <Controls.ActionButton
                  color="primary"
                  onClick={() => setData(item.id, item.name, item.username, item.email, item.gender, item.phone, item.city, item.state, item.password)}
                >
                  <EditOutlinedIcon fontSize="small" />
                </Controls.ActionButton>
                <Controls.ActionButton
                  color="secondary"
                  onClick={() => {checker(item.id)}}
                  // setConfirmDialog({
                  //   isOpen: true,
                  //   title: "Are you sure to delete this record?",
                  //   subTitle: "You can't undo this operation",
                  //   // onConfirm: () => {
                  //   //   onDelete(item.id);
                  //   // },
                  // });
                >
                  <CloseIcon fontSize="small" />
                </Controls.ActionButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default UserList;

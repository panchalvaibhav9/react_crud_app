import React, { useEffect } from 'react';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import LockIcon from '@mui/icons-material/Lock';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';



const Login = () => {

  const [apiData, SetApiData] = React.useState([]);

  useEffect(() => {
    axios.get('https://6256c4796ea70370053eff99.mockapi.io/mainapi')
      .then(res => {
        SetApiData(res.data);
        console.log(res.data);
      })
  }, [])

  const navigate = useNavigate();

  const paperStyle = { padding: 20, width: 340 }
  const avatarStyle = { backgroundColor: '#00073D' }
  const btnstyle = { margin: '20px 0', backgroundColor: '#00073D' }
  const gridStyle = { margin: '50px auto', height: '73vh' }

  const initialvalues = {
    username1: '',
    password1: ''
  }

  const onSubmit = (values, props) => {
    console.log(values)
    console.log(props)
    console.log(apiData.map(data => data.username))
    console.log(values.username1 === apiData.map(data => data.username))
    console.log(values.username1)
    console.log(values.password1)
    // if(values.username1 === apiData.username && values.password1 === apiData.password) {
      if(values.username1 === apiData.map(data => data.username) && values.password1 === apiData.map(data => data.password)) {
      alert('Login Successful');
      navigate('/dashboard');
    }
    else {
      alert('Login Failed');
    }
  }

  const validationSchema = Yup.object().shape({
    username1: Yup.string().min(3, 'Its too short').required("Required field"),
    password1: Yup.string().min(8, 'Password minimum lenght should be 8').required("Required field")
  })

  return (
    <Grid>
      <Paper style={paperStyle}>
        <Grid align='center' style={gridStyle}>
          <Avatar style={avatarStyle}><LockIcon /></Avatar>
          <h2>Log In</h2>
          <Typography variant='caption' gutterBottom>Please do login for accessing website !</Typography>
          <Formik initialValues={initialvalues} onSubmit={onSubmit} validationSchema={validationSchema}>
            {(props) => (
              <Form>
                <Field as={TextField} label='Username' variant="standard" name='username1' placeholder='Enter username' fullWidth helperText={<ErrorMessage name='username1' />} />
                <Field as={TextField} label='Password' variant="standard" name='password1' placeholder='Enter password' type='password' fullWidth helperText={<ErrorMessage name='password1' />} />
                <Button type='submit' variant='contained' backgroundColor='#00073D' style={btnstyle} fullWidth>Log In</Button>
              </Form>
            )}
          </Formik>
          <Typography >
            <Link href="#" >
              Forgot password ?
            </Link>
          </Typography>
          <Typography > Do you have an account ?
            <Link href="#" >
              Sign Up
            </Link>
          </Typography>
        </Grid >
      </Paper>
    </Grid>
  )
}

export default Login

import React, { useEffect, useState } from "react";
import { Grid, Paper } from "@mui/material";
import Controls from "../Controls";
import { Field, Form, Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Updateusers = () => {
    const genderItems = [
        { id: "male", title: "Male" },
        { id: "female", title: "Female" },
        { id: "other", title: "Other" },
    ];
    
    const navigate = useNavigate();
    const [usersData, setUsersDate] = useState("");
    const [id] = useState(usersData.id)
    
    useEffect(() => {
      setUsersDate(JSON.parse(localStorage.getItem("updateUser")));
      // const data = localStorage.getItem("updateUser");
      // console.log(data);
    }, []);
    

  const initialValues = {
    fullname: "",
    username: "",
    email: "",
    gender: "male",
    phone: "",
    city: "",
    state: "",
    password: "",
    conpassword: "",
  };

  const validationSchema = Yup.object().shape({
    fullname: Yup.string().min(3, "Its too short").required("Required field"),
    username: Yup.string().min(3, "Its too short").required("Required field"),
    email: Yup.string().email("Invalid email").required("Required field"),
    phone: Yup.string().min(10, "Its too short").required("Required field"),
    city: Yup.string().min(3, "Its too short").required("Required field"),
    state: Yup.string().min(3, "Its too short").required("Required field"),
    password: Yup.string()
      .min(8, "Password minimum lenght should be 8")
      .required("Required field"),
    conpassword: Yup.string()
      .oneOf([Yup.ref("password"), null], "Password does not match")
      .required("Required field"),
  });

  const onSubmit = (values, props) => {
    console.log(values);
    console.log(props);
    axios
      .put(`https://6256c4796ea70370053eff99.mockapi.io/mainapi${id}`, {
        name: values.fullname,
        username: values.username,
        email: values.email,
        gender: values.gender,
        phone: values.phone,
        city: values.city,
        state: values.state,
        password: values.password,
      })
      .then((res) => {
        console.log(res);
      });
    navigate("/User/UserList");
  };

  console.log(usersData.id);

  return (
    <Paper elevation={11} style={{ margin: 25 }}>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form style={{ margin: "20px 70px" }}>
            <Grid
              container
              direction="column"
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={6} style={{ marginTop: 15 }}>
                <Field
                  as={Controls.Input}
                  name="fullname"
                  label="Full Name"
                  style={{ width: "45%", margin: "10px 15px" }}
                  helperText={<ErrorMessage name="fullname" />}
                />
                <Field
                  as={Controls.Input}
                  name="username"
                  label="User Name"
                  values={usersData.username}
                  style={{ width: "45%", margin: "10px 15px" }}
                  helperText={<ErrorMessage name="username" />}
                />
                <Field
                  as={Controls.Input}
                  name="email"
                  label="E-Mail"
                  values={usersData.email}
                  helperText={<ErrorMessage name="email" />}
                  style={{ width: "93%", margin: "10px 15px" }}
                />
                <Field
                  as={Controls.Input}
                  name="phone"
                  label="Phone Number"
                  helperText={<ErrorMessage name="phone" />}
                  style={{ width: "45%", margin: "10px 15px" }}
                />
                <Field
                  as={Controls.RadioGroup}
                  name="gender"
                  label="Gender"
                  helperText={<ErrorMessage name="gender" />}
                  items={genderItems}
                />
                <Field
                  as={Controls.Input}
                  name="city"
                  label="Your City"
                  helperText={<ErrorMessage name="city" />}
                  style={{ width: "45%", margin: "10px 15px" }}
                />
                <Field
                  as={Controls.Input}
                  name="state"
                  label="Your State"
                  helperText={<ErrorMessage name="state" />}
                  style={{ width: "45%", margin: "10px 15px" }}
                />
                <Field
                  as={Controls.Input}
                  name="password"
                  label="Password"
                  type="password"
                  helperText={<ErrorMessage name="password" />}
                  style={{ width: "45%", margin: "10px 15px" }}
                />
                <Field
                  as={Controls.Input}
                  name="conpassword"
                  label="Confirm Password"
                  type="password"
                  helperText={<ErrorMessage name="conpassword" />}
                  style={{ width: "45%", margin: "10px 15px" }}
                />
              </Grid>
              <Grid align="center">
                <Controls.Button
                  type="submit"
                  text="Update"
                  style={{ margin: "25px" }}
                  disabled={props.isSubmitting}
                />
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </Paper>
  );
};

export default Updateusers;

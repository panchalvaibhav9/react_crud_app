import React from "react";
import {
  Grid,
  Paper,
  Avatar,
  Typography,
  Button,
  RadioGroup,
  FormControlLabel,
  Radio,
  FormLabel,
  FormControl,
  Checkbox,
  TextField,
} from "@mui/material";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { FormHelperText } from "@mui/material";

const Registration = () => {
  const paperStyle = { padding: 15, width: 340, margin: "0 auto" };
  const headerStyle = { margin: 0 };
  const avatarStyle = { backgroundColor: "#00073D" };
  const marginTop = { marginTop: 2 };
  const btnstyle = { margin: "0px 0", backgroundColor: "#00073D" };

  const initialvalues = {
    name: "",
    username: "",
    email: "",
    gender: "",
    phone: "",
    password: "",
    conpassword: "",
    termandcondition: false,
  };

  const onSubmit = (values, props) => {
    console.log(values);
    console.log(props);
    setTimeout(() => {
      props.resetForm();
      props.setSubmitting(false);
    }, 2000);
    axios
      .post("https://6256c4796ea70370053eff99.mockapi.io/mainapi", {
        name: values.fullname,
        username: values.username,
        email: values.email,
        gender: values.gender,
        phone: values.phone,
        password: values.conpassword
      })
      .then((res) => {
        console.log(res);
      });
  };

  const validationSchema = Yup.object().shape({
    name: Yup.string().min(3, "Its too short").required("Required field"),
    username: Yup.string().min(3, "Its too short").required("Required field"),
    email: Yup.string().email("Enter valid email").required("Required field"),
    phone: Yup.number()
      .typeError("Enter valid phone number")
      .required("Required field"),
    password: Yup.string()
      .min(8, "Password minimum lenght should be 8")
      .required("Required field"),
    conpassword: Yup.string()
      .oneOf([Yup.ref("password"), null], "Password does not match")
      .required("Required field"),
    termandcondition: Yup.boolean()
      .oneOf([true], "You must accept terms and conditions")
      .required("Required field"),
  });

  return (
    <Grid>
      <Paper style={paperStyle}>
        <Grid align="center">
          <Avatar style={avatarStyle}>
            <AddCircleOutlinedIcon />
          </Avatar>
          <h2 style={headerStyle}>Registration</h2>
          <Typography variant="caption" gutterBottom>
            Please fill this form to create an account !
          </Typography>
        </Grid>
        <Formik
          initialValues={initialvalues}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {(props) => (
            <Form>
              <Field
                as={TextField}
                fullWidth
                variant="standard"
                name="name"
                label="Name"
                placeholder="Enter your name"
                helperText={
                  <ErrorMessage name="name" style={{ color: "red" }} />
                }
              />

              <Field
                as={TextField}
                fullWidth
                variant="standard"
                name="username"
                label="Username"
                placeholder="Enter username for login"
                helperText={<ErrorMessage name="username" />}
              />

              <Field
                as={TextField}
                fullWidth
                variant="standard"
                name="email"
                label="Email"
                placeholder="Enter your email"
                helperText={<ErrorMessage name="email" />}
              />

              <FormControl component="fieldset" style={marginTop}>
                <FormLabel component="legend">Gender</FormLabel>
                <Field
                  as={RadioGroup}
                  aria-label="gender"
                  name="gender"
                  style={{ display: "initial" }}
                >
                  <FormControlLabel
                    value="female"
                    control={<Radio />}
                    label="Female"
                  />
                  <FormControlLabel
                    value="male"
                    control={<Radio />}
                    label="Male"
                  />
                </Field>
              </FormControl>
              <FormHelperText>
                <ErrorMessage name="gender" />
              </FormHelperText>

              <Field
                as={TextField}
                fullWidth
                variant="standard"
                name="phone"
                label="Phone Number"
                placeholder="Enter your phone number"
                helperText={<ErrorMessage name="phone" />}
              />

              <Field
                as={TextField}
                fullWidth
                variant="standard"
                type="password"
                name="password"
                label="Password"
                placeholder="Enter your password"
                helperText={<ErrorMessage name="password" />}
              />

              <Field
                as={TextField}
                fullWidth
                variant="standard"
                type="password"
                name="conpassword"
                label="Confirm Password"
                placeholder="Confirm your password"
                helperText={<ErrorMessage name="conpassword" />}
              />

              <FormControlLabel
                control={<Field as={Checkbox} name="termandcondition" />}
                label="I accept the terms and conditions."
              />
              <FormHelperText>
                <ErrorMessage name="termandcondition" />
              </FormHelperText>

              <Button
                type="submit"
                variant="contained"
                backgroundColor="#00073D"
                style={btnstyle}
                disabled={props.isSubmitting}
                fullWidth
              >
                {props.isSubmitting ? "Registered" : "Register"}
              </Button>
            </Form>
          )}
        </Formik>
      </Paper>
    </Grid>
  );
};

export default Registration;

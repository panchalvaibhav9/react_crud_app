import "./App.css";
import { Route, Routes } from "react-router-dom";
import { CssBaseline } from "@mui/material";
import Sidebar from "./Components/Sidebar/Sidebar";
import Dashboard from "./Components/Dashboard";
import AddUser from "./Components/AddUser";
import UserList from "./Components/UserList";
import AuthTabs from "./Components/AuthTabs";
import "react-toastify/dist/ReactToastify.css";
import Updateusers from "./Components/Updateusers";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<AuthTabs />} />
      </Routes>
      <Sidebar>
        <Routes>
          <Route path="/Dashboard" element={<Dashboard />} />
          <Route path="/User/Adduser" element={<AddUser />} />
          <Route path="/User/UserList" element={<UserList />} />
            <Route path="/User/Updateuser" exact element={<Updateusers />} />
        </Routes>
      </Sidebar>
      <CssBaseline />
    </>
  );
}

export default App;

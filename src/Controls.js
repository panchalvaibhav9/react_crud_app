import ActionButton from "./Controls/ActionButton";
import Button from "./Controls/Button";
import Checkbox from "./Controls/Checkbox";
import Select from "./Controls/Select";
import Input from "./Controls/Input";
import RadioGroup from "./Controls/RedioGroup";
import DatePicker from "./Controls/DatePicker";


const Controls = {
    Input,
    RadioGroup,
    Select,
    Checkbox,
    DatePicker,
    Button,
    ActionButton
}

export default Controls;